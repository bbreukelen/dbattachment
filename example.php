<?PHP
//// Required in config vars:
// For simplePDO
$config['dbhost']    = 'hostname';
$config['dbuser']    = 'username';
$config['dbpwd']     = 'password';
$config['dbname']    = 'dbase name';
$config['dbtz']      = 'Europe/Amsterdam';
$config['dbcharset'] = 'UTF8';

// For dbAttachment
$config['dbAttachment']['attachmentsPath'] = "/tmp/dbAttachment";


include 'simplePDO/simplePDO.class.php';
include 'dbattachment/dbAttachment.class.php';

$db = new simplePDO();
$dbAttachment = new dbAttachment();

// Saving
$dbAttachment->save( '/tmp/test_photo.png', 'image', 'test_photo.png', Array('width' => '320') );
// or 
$dbAttachment->save( '', 'image', 'test_photo.png', Array('width' => '320'), $binary_data );
// metadata is optional and must be an array if provided. The file_path isusually $_FILES['x']['tmp_name']
// binary is optional but if set, the file can be parsed into the function and will not be read from input file
// returns -1 or the id of the record

// Getting
$dbAttachment->get( 'xxyyzz' ); // returns -1 or the content of the file
$dbAttachment->getBase64( 'xxyyzz' ); // returns -1 or the content of the file base64 encoded
$dbAttachment->download( 'xxyyzz' ); // returns -1 or the content of the file but forces download in browser

$dbAttachment->getMeta( 'xxyyzz' ); // returns -1 or an aboject with filename, filesize and meta_data (object) for the file

// Deleting
$dbAttachment->remove( 'xxyyzz' ); // returns nothing
?>
