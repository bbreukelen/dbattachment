CREATE TABLE `attachments` (
  `id` char(36) NOT NULL DEFAULT '',
  `type` char(50) DEFAULT NULL,
  `name` char(128) DEFAULT NULL,
  `size` float DEFAULT NULL,
  `meta_data` longtext,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
