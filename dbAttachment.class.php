<?
////////////////////////////////////////////////////////////////////////
//
//   ___     _                  ___       __ _                       
//  | __|  _| |_ _  _ _ _ ___  / __| ___ / _| |___ __ ____ _ _ _ ___ 
//  | _| || |  _| || | '_/ -_) \__ \/ _ \  _|  _\ V  V / _` | '_/ -_)
//  |_| \_,_|\__|\_,_|_| \___| |___/\___/_|  \__|\_/\_/\__,_|_| \___|
//                                                                  
// 2015-01-22 :: futuresoftware.nl
////////////////////////////////////////////////////////////////////////
//
class dbAttachment {
	private $appPath;

	///////////////////////////////////////
	public function __construct() {
		global $config;
		$this->appPath = $config['dbAttachment']['attachmentsPath'];
	}

	public function get($id) {
		global $main, $config, $db, $main;
		// Pulls a file and returns the content
		$sql = sprintf("SELECT id FROM attachments WHERE id = '%s'", $db->esc($id) );
		if (! $db->get_row($sql, 'id')) { $this->log("No record exists for attachment: {$id}", 'ERROR'); return -1; }

		$filePath = $this->appPath . '/' . $id;
		if (! is_readable($filePath)) { $this->log("File does not exist or is not readable: {$id}", 'ERROR'); return -1; }
		return @file_get_contents($filePath);
	}

	public function getBase64($id) {
		$bin = $this->get($id);
        if ($bin == -1) { return -1; }
		return base64_encode($bin);
	}

	public function download($id) {
		global $main, $config, $db, $main;
		// Pulls a file and offers as download
		if (ob_get_contents()) { return -1; } // Cannot output a file for download when already outputted something

		$sql = sprintf("SELECT id,name FROM attachments WHERE id = '%s'", $db->esc($id) );
		$file = $db->get_row($sql);
		if (! $file->id) { $this->log("No record exists for attachment: {$id}", 'ERROR'); return -1; }
		$filePath = $this->appPath . '/' . $file->id;
		if (! is_readable($filePath)) { $this->log("File does not exist or is not readable: {$id}", 'ERROR'); return -1; }

		$file->data = @file_get_contents($filePath);

		header('Content-Description: File Transfer');
		if (headers_sent()) { return -1; }
		header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		// force download dialog
		header('Content-Type: application/force-download');
		header('Content-Type: application/octet-stream', false);
		header('Content-Type: application/download', false);
		header('Content-Type: application/pdf', false);
		// use the Content-Disposition header to supply a recommended filename
		header('Content-Disposition: attachment; filename="'.$file->name.'";');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.strlen($file->data));
		echo $file->data;
		unset($file);
		exit(0);
	}

	public function getMeta($id) {
		global $main, $config, $db, $main;
		// Retrieves the meta data of an attachment along with the filename and size
		$sql = sprintf("SELECT id, `name` filename, size filesize, meta_data FROM attachments WHERE id = '%s'", $db->esc($id) );
		$data = $db->get_row($sql);
		if (! $data || ! $data->id) { $this->log("No record exists for attachment: {$id}", 'ERROR'); return -1; }
		$data->meta_data = @json_decode($data->meta_data);
		return $data;
	}

	public function save($oldPath, $type, $filename, $meta=Array(), $binary='') {
		global $main, $config, $db, $main;
		// Stores file on disk and makes db record
		if (! isset($db)) { $this->log("No database method", 'ERROR'); return -1; }
		if (! $oldPath && $binary=='') { $this->log("No attachment found", 'ERROR'); return -1; }
		if (! $filename) { $this->log("No filename found", 'ERROR'); return -1; }

		$uuid = $db->uuid();
		if (! $uuid) { $this->log("Did not retrieve UUID from database", 'ERROR'); return -1; }
		$bin = ($binary == '' ? 0 : 1);

		// Move file to final position
		$newPath = $this->appPath . '/' . $uuid;

		if ($bin) {
			$ret = @file_put_contents($newPath, $binary);
			if ($ret === false) { $this->log("Error writing attachment file {$newPath}", 'ERROR'); return -1; }
			@chmod($newPath, 0644); // Make sure webserver can read the file
		} else {
			$ret = @rename($oldPath, $newPath);
			if ($ret === false) { $this->log("Error moving file {$oldPath}", 'ERROR'); return -1; }
		}

		$meta = @json_encode($meta);
		$sql = sprintf("INSERT INTO attachments (id, type, name, size, meta_data, created_date) VALUES ('%s', '%s', '%s', '%f', '%s', NOW() )",
			$uuid,
			$db->esc($type),
			$db->esc($filename),
			($bin ? strlen($binary) : filesize($oldPath) ),
			$db->esc($meta)
		);
		$ret = $db->push_data($sql);
		if (! $ret) {
			$this->log("Error saving attachment database record for {$uuid}", 'ERROR');
			return -1;
		}
		unset($binary);
		return $uuid;
	}

	public function remove($id) {
		global $main, $config, $db, $main;
		$filePath = $this->appPath . '/' . $id;
		$sql = sprintf("DELETE FROM attachments WHERE id = '%s'", $db->esc($id) );
		$ret = $db->push_data($sql);
		if ($ret) {
			if (file_exists($filePath)) {
				@unlink($filePath);
			}
		}
	}

	//////////////////////////////////////////
	private function log($msg, $level) {
		global $logIt;
		if (isset($logIt)) {
			$logIt->log("dbAttachment - {$msg}", $level);
		}
	}

}
?>
